" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2011 Apr 15
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
    finish
endif

" File Path Settings
let $MEETING = "C:/Users/TANAKA-LAB/work/meeting"
let $DL = "C:/Users/TANAKA-LAB/Downloads"
let $SETTING = "C:/Users/TANAKA-LAB/.vim/Settings"

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start
set nu
set shiftwidth=4	" set indent to 3 space
set softtabstop=4	" set tab is 3 space
set autoindent	" always set autoindenting on
set nocursorline " change the color of the line
set nocursorcolumn " change the is visiblecolor of the raw
set laststatus=2 " the bottom of the status line is visible
set helpheight=999
set nolist " unvisible the unbisible caractor
set whichwrap=b,s,<,>,[,] "enable to the move bitween the last of line and the top of the line
set shellslash " use the / altanative of back/
set wildmenu  " enable to the search in the command line
set history=10000 " the number of saving at command line mode

if has("vms")
    set nobackup		" do not keep a backup file, use versions instead
else
    set backup		" keep a backup file
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set clipboard=unnamed,unnamedplus	" clipboard = windows
set wrap " turn the line if over the window
set ignorecase smartcase " search smartcase
set nohlsearch		" don't use hilight search
set wildmenu
set showmatch
source $VIMRUNTIME/macros/matchit.vim

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
	au!

	" For all text files set 'textwidth' to 78 characters.
	autocmd FileType text setlocal textwidth=78

	" When editing a file, always jump to the last known cursor position.
	" Don't do it when the position is invalid or when inside an event handler
	" (happens when dropping a file on gvim).
	" Also don't do it when the mark is in the first line, that is the default
	" position when opening a file.
	autocmd BufReadPost *
		    \ if line("'\"") > 1 && line("'\"") <= line("$") |
		    \   exe "normal! g`\"" |
		    \ endif

    augroup END

"    augroup swapchoice-readonly
"	autocmd!
"	autocmd SwapExists * let v:swapchoice = 'o'
"    augroup END

else


endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		\ | wincmd p | diffthis
endif

au BufRead,BufNewFile *.md set filetype=markdown
au BufRead,BufNewFile *.tex,*.latex,*.sty,*.dtx,*.ltx,*.bbl set filetype=tex
filetype plugin on
set shellslash
set grepprg=grep\ -nH\ $*
filetype indent on

" DEIN Settings
"if &compatible
"    set nocompatible
"endif
"set runtimepath+=~/.vim/dein/dein.vim
"call dein#begin(expand('~/.vim/dein'))
"call dein#add('kannokanno/previm.vim')
"call dein#add('syui/cscroll.vim')
"call dein#add('Shougo/neocomplete.vim')
"call dein#add('Shougo/unite.vim')
"call dein#add('Shougo/vimfiler')
"call dein#end()

" KEY MAPPING
nn j gj
nn gj j
nn k gk
nn gk k
vn j gj
vn gj j
vn k gk
vn gk k
nn Y y$
nn <C-k> :h 
nn <S-k> :call cursor((3*line("w0")+line("w$"))/4,0)<CR>
nn <S-j> :call cursor((line("w0")+3*line("w$"))/4,0)<CR>
"nn <C-j> <S-j>
nn gc :call cursor(0,strlen(getline("."))/2)<CR>
nn gC gm
nn gm `m
nn gj J
"nn tt <C-]>
"nn tj :<C-u>tab<CR>
"nn tk :<C-u>pop<CR>
"nn tl :<C-u>tags<CR>
"nn <C-CR> o<Esc>
nn <S-CR> O<Esc>
nn <Space>l :!ls<CR>
nn <Space>cu :e ++enc=utf-8<CR>
nn <Space>. :tabf $MYVIMRC<CR>
nn <Space>cs :source $MYVIMRC<CR>
nn <Space>cf :e $HOME/vimfiles/ftplugin/
nn <Space>es :e $SETTING/
nn <Space>ct :!ctags -R<CR>
nn <Space>t :tabf %<CR>
nn <Space>b :%!xxd<CR>
"nn <Space>f :!start %:h<CR>
ino <silent>jj <ESC>
ino <silent>���� <ESC>
ino { <C-]>{}<Esc>i
ino {<BS> <C-]>{<BS>
ino {<CR> <C-]>{}<Esc>i<CR><Esc><S-o>
ino <A-{> {
nn g{ bi{<Esc>ea}
nn <space>{ i{<Esc>la}
im ( ()<Esc>i
ino <A-(> (
nn g( bi(<Esc>ea)
nn <space>( i(<Esc>la)
im [ []<Esc>i
ino <A-[> [
nn g[ bi[<Esc>ea]
nn g$ bi$<Esc>ea$
nn <space>[ i[<Esc>la]
ino <A-=> <Space>=<Space>
ino <A-+> <Space>+<Space>
ino <A--> <Space>-<Space>
ino <C-k> <Esc>O
ino <A-j> <Down>
ino <A-k> <Up>
ino <A-h> <Left>
ino <A-l> <Right>
cm <tab> <C-l>

" git command
nn <Space>gi :!git init<CR>
nn <Space>ga :!git add %<CR>
nn <Space>gs :!git status -s<CR>
nn <Space>gc :!git commit -m ""<Left>
nn <Space>gl :!git log<CR>
nn <Space>gd :!git diff --cached<CR>

" New command
" command qv 
" command qs

" Abbreviations

" Unite.vim
"let g:unite_enable_start_insert = 1
"nn <Space>u :Unite 
"nn <Space>f :Unite file<CR>

" COLOR SCHEME

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif
colorscheme myclr

